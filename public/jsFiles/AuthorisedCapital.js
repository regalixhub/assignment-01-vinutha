fetch('AuthorisedCapital.json')
  .then(resp => resp.json())
  .then((data) => {
    Highcharts.chart('container0', {
      chart: { type: 'column' },
      title: { text: 'Authorised Capital' },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif',
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Registered Companies Count',
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: 'Number of companies have Capital: <b>{point.y:.1f} millions</b>',
      },
      series: [{
        name: 'Population',
        data: [
          ['< 1L', data[0]],
          ['1L to 10L', data[1]],
          ['10L to 1Cr', data[2]],
          ['10Cr to 100Cr', data[3]],
          ['More than 100Cr', data[4]],
        ],
        colorByPoint: true,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y:.1f}',
          y: 10,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif',
          },
        },
      }],
    });
  });
