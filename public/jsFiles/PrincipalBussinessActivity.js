fetch('PrincipalBussinessActivity.json')
  .then(resp => resp.json())
  .then((data) => {
    const keysSorted = Object.keys(data).sort((a, b) => data[b] - data[a]);
    const pba = [];
    const reg = [];
    for (let i = 0; i < 10; i += 1) {
      pba.push(keysSorted[i]);
      reg.push(data[keysSorted[i]]);
    }
    Highcharts.chart('container2',
      {
        chart:
        { type: 'column' },
        title: { text: 'Registrations by PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN for the year 2015' },
        xAxis: {
          categories: pba,
        },
        yAxis: {
          min: 0,
          title: { text: 'PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN' },
        },
        legend: { enabled: false },
        tooltip:
         { pointFormat: 'Companies have PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN : <b>{point.y:.1f}</b>' },
        series: [{
          name: 'Population',
          data: reg,
          colorByPoint: true,
          dataLabels: {
            enabled: true, rotation: -45, color: '#FFFFFF', align: 'right', format: '{point.y:.1f}', y: 10, style: { fontSize: '13px', fontFamily: 'Verdana, sans-serif' },
          },
        }],
      });
  });
